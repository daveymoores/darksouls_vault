var path = require('path');
module.exports = {
   cache: true,
   debug: true,
   devtool: 'eval',
   entry: './src/app.js',
   output: {
      path: path.join(__dirname, "build"),
      filename: 'build.min.js'
   },
   module: {
    loaders: [
      {
       test: /isotope\-|fizzy\-ui\-utils|desandro\-|masonry|outlayer|get\-size|doc\-ready|eventie|eventemitter|classie|get\-style\-property|packery/,
       loader: 'imports?define=>false&this=>window'
     }
    ]
   },
   resolve: {
      extensions: ['', '.js', '.json', '.coffee']
   }
};
