var $ = require("jquery");
var shareUrl = 'http://uk-microsites.ign.com/dark-souls-archives';
var Share = require('./share');

function Fb () {

   this.$window = $(window);
   this.$window.on('load', $.proxy(this.init, this));

};

Fb.prototype.init = function(){

   new Share(".share_btn", {
     ui: {
       flyout: "bottom center"
     },
     networks: {
       facebook: {
         app_id: "1670553699829010",
         url: shareUrl
       },
       google_plus: {
         url: shareUrl
       },
       twitter: {
         url: shareUrl
       },
       pinterest: {
         url: shareUrl
       }
     }
   });

};

module.exports = Fb;
