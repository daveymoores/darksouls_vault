<?php
error_reporting(0);

include_once('HkCompetition.class.php');
$comp = new HkCompetition('ign_dark_souls');

if ($comp->form_posted())
{
  // Concatenate the date of birth
  // $dob = $comp->post('dob_y')."-".$comp->post('dob_m')."-".$comp->post('dob_d');

  $comp->set_data(array(
    'answers' => $comp->post('answers'),
    'fname' => $comp->post('fname'),
    'lname' => $comp->post('lname'),
    'email' => $comp->post('email'),
    //'dob' => '2015-01-01',
    'terms' => $comp->post('terms'),
    'subscribe' => $comp->post('subscribe')));

  if ($comp->submit())
  {
    echo json_encode(array(
      'success' => TRUE));
  }
  else {
    echo json_encode(array(
      'success' => FALSE,
      'error' => "Sorry, the form couldn't be submitted. This is our problem and we are working to fix it. Please try again later."));
  }
}

// If form not posted
else {
  echo json_encode(array(
    'success' => FALSE,
    'error' => "Direct script access not allowed"));
}
