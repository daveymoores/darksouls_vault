<?php

define('EMAIL_REGEX', '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/');

class HkCompetition {

	private $_campaign_name = '';
	private $_errors = array(); // Stores validation errors

	private $_defaults = array(
		'days' => array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
		  "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23",
		  "24", "25", "26", "27", "28", "29", "30", "31"),

		'months' => array("January", "February", "March", "April", "May", "June",
  		"July", "August", "September", "October", "November", "December"),

		'years' => array("2009", "2008", "2007", "2006", "2005", "2004", "2003",
		  "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994",
		  "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985",
		  "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976",
		  "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967",
		  "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958",
		  "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949",
		  "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940",
		  "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931",
		  "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922",
		  "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913",
		  "1912", "1911", "1910", "1909")
	);

	private $_data = array(); // Data to post to HK

	public function __construct($campaign_name) {
		$this->_campaign_name = $campaign_name;
	}

	// Private methods

	private function _value_posted($field_name, $value)
	{
		if ($value == NULL)
		{
			if (isset($_POST[$field_name])) {
	      return TRUE;
	    }
	    else {
	      return FALSE;
	    }
		}

		else 
		{
			$field_name = str_replace(']', '', $field_name);
			$field_exploded = explode('[', $field_name);

			if (count($field_exploded) <= 1) {
				return FALSE;
			}

			$field_name = $field_exploded[0];
			$index = $field_exploded[1];

	    if (isset($_POST[$field_name])) 
	    {
	      if (isset($_POST[$field_name][$index])) 
	      {
	        if ($_POST[$field_name][$index] == $value) {
	          return TRUE;
	        }
	        else {
	          return FALSE;
	        }
	      } 
	      else {
	        return FALSE;
	      }
	    }
	    else {
	      return FALSE;
	    }
		}
	}
 	
	// Public instance methods

	public function set_data($data)
	{
		foreach ($data as $field => $value)
		{
			// If there's a validation rule
			if (is_array($value))
			{
				$validation_rules = $value[1];
				$value = $value[0];

				foreach ($validation_rules as $rule => $message)
				{
					if ($rule == 'required')
					{
						if ($value === FALSE || empty($value)) {
							$this->_errors[$field] = $message;
							break;
						}
					}

					if ($rule == 'valid_email')
					{
						if (!preg_match(EMAIL_REGEX, $value))
						{
							$this->_errors[$field] = $message;
							break;
						}
					}

					if ($rule == 'valid_date')
					{
						try {
						  $date = new DateTime($value);
						} catch (Exception $e) {
						  $this->_errors[$field] = $message;
							break;
						}

						$value = $date->format('Y-m-d');
					}

					if ($rule == 'over_18')
					{
						try {
						  $date = new DateTime($value);
						  $time = $date->getTimestamp();
						  $eighteen_years = 31536000 * 18;
						  $now = time();

						  if (($now - $time) < $eighteen_years) {
						  	$this->_errors[$field] = $message;
								break;
						  }
						} 
						catch (Exception $e) {
						  $this->_errors[$field] = $message;
							break;
						}
					}

					if ($value == 'on') $value = 1;
					$this->_data[$field] = $value;
				}
			}

			// If there's no validation rule
			else {
				if ($value == 'on') $value = 1;
				$this->_data[$field] = $value;
			}
		}
	}

	public function is_valid()
	{
		return (count($this->_errors) == 0);
	}

	public function submit($campaign)
	{
    $ch = curl_init();
    
    // check for a campaign name
    if(empty($this->_campaign_name)) 
    {        
      throw new Exception("Campaign name is missing. Please pass the campaign name when instantiating this class. Eg: new HkCompetition('campaign_name').");
    }

    // Set the campaign name
    $this->_data['_campaign'] = $this->_campaign_name;

    // set options
    curl_setopt($ch, CURLOPT_URL, "http://harkable-clients-v2.gopagoda.io/ign_api/api.php");
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'admin:igngames');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_data);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    
    $output = curl_exec($ch);

    // close curl resource to free up resource
    curl_close($ch);

    // return the result
    $result = unserialize($output);

    if (isset($result['_auth']) && $result['_auth'] == 'failed')
    {
    	return FALSE;
    }

    if(isset($result['_saved']) == TRUE) 
    {
    	return TRUE;
    }
    else {
    	return FALSE;
    }
	}

	// Public Utilities

	public function formdata($key) 
	{
		if (isset($this->_defaults[$key])) {
			return $this->_defaults[$key];
		}
		else {
			return FALSE;
		}
	}

	public function post($field_name)
	{
		if (strpos($field_name, '[') !== false)
		{
			$field_name = str_replace(']', '', $field_name);
			list($key, $index) = explode('[', $field_name);

			if (isset($_POST[$key])) {
				if (isset($_POST[$key][$index])) {
					return $_POST[$key][$index];
				}
				else {
					return FALSE;
				}
			}
			else {
				return FALSE;
			}
		}
		else if (isset($_POST[$field_name])) 
		{
			return $_POST[$field_name];
		}
		else {
			return FALSE;
		}
	}

	public function error($field_name)
	{
		if (isset($this->_errors[$field_name])) {
	    return $this->_errors[$field_name];
	  }
	  else {
	    return FALSE;
	  }
	}

	public function has_error($field_name)
	{
		if (isset($this->_errors[$field_name])) {
	    return TRUE;
	  }
	  else {
	    return FALSE;
	  }
	}

	public function has_errors()
	{
		return (count($this->_errors) > 0);
	}

	public function value($field_name) 
	{
		if (isset($_POST[$field_name])) {
	    return $_POST[$field_name];
	  }
	  else {
	    return "";
	  }
	}

	public function checked($field_name, $value = NULL)
	{
		if ($this->_value_posted($field_name, $value)) {
			return 'checked="checked"';
		}
		else {
			return "";
		}
	}

	public function selected($field_name, $value = NULL)
	{
		if ($this->_value_posted($field_name, $value)) {
			return 'selected';
		}
		else {
			return "";
		}
	}

	public function form_posted()
	{
		return (count($_POST) > 0);
	}
}