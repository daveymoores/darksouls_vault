<?php include_once('./lib/functions.php') ?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Dark Souls Archives</title>
    <link rel="shortcut icon" href="favicon.png">
    <link rel="apple-touch-icon" href="dist/build/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="dist/build/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="dist/build/img/apple-touch-icon-114x114.png">
    <!-- Open Graph-->
    <meta property="og:title" content="The Dark Souls Archives">
    <meta property="og:site_name" content="The Dark Souls Archives">
    <meta property="og:url" content="http://uk-microsites.ign.com/dark-souls-archives">
    <meta property="og:image" content="http://uk-microsites.ign.com/dark-souls-archives/dist/build/img/ogimage.jpg">
    <meta property="og:description" content="Get ready to embrace the darkness with the Dark Souls Archives">
    <meta property="fb:app_id" content="1670553699829010">
    <link rel="stylesheet" href="dist/build/css/style.css">
    <style id="countries" type="text/css">._gb, ._au, ._other { display: none!important; }</style>
  </head>
  <body>
    <div data-offcanvas class="off-canvas-wrap">
      <div class="inner-wrap">
        <nav id="topNav" data-topbar role="navigation" class="tab-bar tab-bar-red top">
          <section class="left-small show-for-small-only"><a href="#" class="left-off-canvas-toggle menu-icon"><span></span></a></section>
          <div class="row">
            <div class="ign_red-wrapper">
              <h1 class="title"><a href="http://www.ign.com" target="_blank"><img src="dist/build/img/boilerplate/ign-logo.png" alt="IGN logo"></a></h1>
            </div>
            <ul id="share_override" class="left share_btn">
              <li><a href="#"></a></li>
            </ul><span class="sponsored">Sponsored</span>
            <ul class="right links">
              <li><a href="http://www.game.co.uk/webapp/wcs/stores/servlet/HubArticleView?hubId=923258&articleId=923259&catalogId=10201&langId=44&storeId=10151&cm_mmc=IGN-_-DarkSouls3-_-Website-_-Hub" target="_blank" class="_other" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click']);">Buy Dark Souls III</a></li>
            </ul>
          </div>
        </nav>
        <div id="pageBackground" class="Page-background"></div>
        <div class="Page">
          <div class="Page-entry">

            <div class="Center Center-height">

              <div class="Entry-logo"><img src="dist/build/img/archives_title.png" alt="" /></div>
              <div class="Entry-desc">

                <p class="Entry-text">Darkness is coming on April 12 and below is everything you need to be ready to embrace it.</p>
                <p class="Entry-text">Whether you’re new to Dark Souls or a seasoned player, there is something for you here. Either look through the content as you wish or pull out the content that’s right for you by selecting your experience level on the right.</p>

              </div>

              <div class="pre-order">
                  <a href="http://www.game.co.uk/webapp/wcs/stores/servlet/HubArticleView?hubId=923258&articleId=923259&catalogId=10201&langId=44&storeId=10151&cm_mmc=IGN-_-DarkSouls3-_-Website-_-Hub" target="_blank" onClick="_gaq.push(['_trackEvent', 'pre-order', 'click']);"><img src="dist/build/img/pre-order-1.png" alt="Pre-order Dark Souls 3" /></a>
              </div>

              <div id="levelMap" class="map map-noob">
                <div class="the-map">
                    <div class="clickpoint__container">
                        <a href="#" onClick="_gaq.push(['_trackEvent', 'ds_noob', 'click']);" data-sort="noob" class="clickpoint"></a>
                        <a href="#" onClick="_gaq.push(['_trackEvent', 'ds_average', 'click']);" data-sort="player" class="clickpoint"></a>
                        <a href="#" onClick="_gaq.push(['_trackEvent', 'ds_pro', 'click']);" data-sort="pro" class="clickpoint"></a>
                    </div>
                </div>
                <ul class="switch">
                  <li class="active"><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_noob', 'click']);" data-sort="noob">I’ve not played Dark Souls before</a></li>
                  <li><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_average', 'click']);" data-sort="player">I’ve played a Dark Souls game before</a></li>
                  <li><a href="#" onClick="_gaq.push(['_trackEvent', 'ds_pro', 'click']);" data-sort="pro">I’m really good at all-things Dark Souls</a></li>
                </ul>
              </div>
            </div>
          </div>
          <!--  grid-->
          <div class="Page-grid Center"><a target="_blank" data-pro="0.1" data-player="1" data-noob="0.01" href="http://uk.ign.com/videos/2016/03/24/dark-souls-3-official-accursed-trailer"  data-video="true" class="Grid-item Grid-item--small Grid-accursed" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--trailer">
              <h3 class="Grid-category">Trailer</h3>
              <h2 class="Grid-title">Dark Souls 3 Official Accursed Trailer</h2>
          </div></a><a target="_blank" data-pro="0.9" data-player="1" data-noob="0.01" href="http://uk.ign.com/videos/2016/03/31/dark-souls-3-official-the-witches-animated-trailer"  data-video="true" class="Grid-item Grid-item--small Grid-animated" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--trailer">
              <h3 class="Grid-category">Trailer</h3>
              <h2 class="Grid-title">Dark Souls 3 Gets an Awesome Animated Short</h2>
          </div></a><a target="_blank" data-pro="1.3" data-player="1" data-noob="0.01" href="http://uk.ign.com/articles/2016/04/04/dark-souls-3-review"  class="Grid-item Grid-item--big Grid-review3" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--review">
              <h3 class="Grid-category">Review</h3>
              <h2 class="Grid-title">Dark Souls 3 Review</h2>
              <div class="Grid-text">Dark Souls 3 offers up tons of fierce weapons and a majestic, terrifying new land to explore.</div>
          </div></a><a target="_blank" data-pro="0.2" data-player="0.9" data-noob="0.05" href="http://uk.ign.com/articles/2016/04/04/the-dark-souls-great-sword-is-a-real-life-monster" class="Grid-item Grid-item--small Grid-bigsword" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">The Dark Souls Great Sword Is A Real-Life Monster</h2>
          </div></a><a target="_blank" data-pro="0.9" data-player="0.5" data-noob="0.09" href="http://uk.ign.com/videos/2016/04/06/dark-souls-prepare-to-try-episode-13-demon-ruins" data-video="true" class="Grid-item Grid-item--small Grid-try13" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN vs. Dark Souls - Episode 13: Demon Ruins</h2>
          </div></a><a target="_blank" data-pro="0.8" data-player="0.9" data-noob="0.1" href="http://uk.ign.com/videos/2016/04/05/dark-souls-in-5-minutes" data-video="true" class="Grid-item Grid-item--big Grid-story5mins" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Dark Souls in 5 minutes</h2>
              <div class="Grid-text">The Dark Souls universe is cryptic and confusing, so before Dark Souls 3 arrives, here's a recap of the story so far.</div>
          </div></a><a target="_blank" data-pro="0.9" data-player="1.1" data-noob="0.7" href="http://www.ign.com/videos/2016/04/04/dark-souls-prepare-to-try-episode-12-the-dukes-archives-crystal-cave-and-seath-the-scaleless" data-video="true" class="Grid-item Grid-item--small Grid-try12" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN vs. Dark Souls - Episode 12: The Duke's Archives &amp; Crystal Cave</h2>
          </div></a><a target="_blank" data-pro="0.1" data-player="0.9" data-noob="0.9" href="http://uk.ign.com/videos/2016/04/04/building-a-life-size-dark-souls-3-statue-in-2-minutes" data-video="true" class="Grid-item Grid-item--small Grid-statue" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Building A Life-Size Dark Souls 3 Statue In 5 Minutes</h2>
          </div></a><a target="_blank" data-pro="0.3" data-player="0.4" data-noob="0.1" href="http://www.ign.com/videos/2016/04/01/dark-souls-prepare-to-try-episode-11-new-londo-ruins-the-four-kings" data-video="true" class="Grid-item Grid-item--small Grid-try11" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN vs. Dark Souls - Episode 11: New Londo Ruins &amp; The Four Kings</h2>
          </div></a><a target="_blank" data-pro="1.3" data-player="1" data-noob="0.01" href="http://uk.ign.com/videos/2016/03/24/every-dark-souls-3-boss-so-far" data-video="true" class="Grid-item Grid-item--small Grid-everyboss" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--preview">
              <h3 class="Grid-category">Preview</h3>
              <h2 class="Grid-title">Every Dark Souls 3 Boss So Far</h2>
          </div></a><a target="_blank" data-pro="1.3" data-player="1" data-noob="0.01" href="http://www.ign.com/videos/2016/03/30/dark-souls-prepare-to-try-episode-10-darkroot-sif-the-great-grey-wolf" data-video="true" class="Grid-item Grid-item--big Grid-try10" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs. Dark Souls: Episode 10 – The Butterfly &amp; The Wolf</h2>
              <div class="Grid-text">Toxic darts, dung pies, and fire-breathing dogs are just a few of the challenges facing us today.</div>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://www.ign.com/videos/2016/03/30/dark-souls-prepare-to-try-episode-10-darkroot-sif-the-great-grey-wolf" data-video="true" class="Grid-item Grid-item--small Grid-try8" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs. Dark Souls - Episode 6: Blighttown</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/27/dark-souls-prepare-to-try-episode-8-sens-fortress" class="Grid-item Grid-item--small Grid-try7" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs. Dark Souls - Episode 7: Quelaag's Domain</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/wikis/dark-souls-3/Classes#Starting_Stat_Comparison_Chart" class="Grid-item Grid-item--small Grid-comparison" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--wiki">
              <h3 class="Grid-category">Wiki</h3>
              <h2 class="Grid-title">Dark Souls III Class Comparison Chart</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/27/dark-souls-prepare-to-try-episode-8-sens-fortress" class="Grid-item Grid-item--small Grid-sens" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Prepare to Try: Episode 8 - Sen's Fortress</h2>
          </div></a><a target="_blank" data-pro="0.1" data-player="0.5" data-noob="1" href="http://uk.ign.com/articles/2016/03/23/dark-souls-3-is-the-hardest-dark-souls-yet" class="Grid-item Grid-item--big Grid-hardestever" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--preview">
              <h3 class="Grid-category">Preview</h3>
              <h2 class="Grid-title">DARK SOULS 3 IS THE HARDEST DARK SOULS YET</h2>
              <div class="Grid-text">And I love it.</div>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/25/dark-souls-3-will-make-a-grown-man-cry-up-at-noon-live" class="Grid-item Grid-item--small Grid-cry" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Dark Souls 3 Will Make a Grown Man Cry</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/28/dark-souls-prepare-to-try-episode-9-anor-londo-ornstein-and-smough" class="Grid-item Grid-item--small Grid-try6" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">Prepare to Try: Episode 9 - Anor Londo &amp; Ornstein and Smough</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/24/dark-souls-3-official-accursed-trailer" class="Grid-item Grid-item--small Grid-accursed" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--trailer">
              <h3 class="Grid-category">Trailer</h3>
              <h2 class="Grid-title">Dark Souls III Official Accursed Trailer</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/wikis/dark-souls-3/Weapons" class="Grid-item Grid-item--big Grid-everyweapon" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--preview">
              <h3 class="Grid-category">Preview</h3>
              <h2 class="Grid-title">Every Weapon Discovered So Far</h2>
              <div class="Grid-text">Dark Souls 3 introduces a new combat feature called Weapon Arts, adding special attacks to each of the Weapons in the game.</div>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/21/dark-souls-prepare-to-try-episode-5-the-depths-gaping-dragon" class="Grid-item Grid-item--small Grid-try5" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs Dark Souls Episode 5</h2>
          </div></a><a target="_blank" data-pro="1" data-player="2" data-noob="70" href="http://www.ign.com/videos/2016/03/21/8-dark-souls-secrets-you-probably-didnt-find" class="Grid-item Grid-item--small Grid-secrets" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">8 Dark Souls Details You Probably Never Noticed</h2>
          </div></a><a target="_blank" data-pro="2" data-player="1" data-noob="3" href="http://uk.ign.com/videos/2016/03/16/dark-souls-prepare-to-try-episode-2-the-undead-burg" class="Grid-item Grid-item--small Grid-try2" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs. Dark Souls - Episode 2: The Undead Burg</h2>
          </div></a><a target="_blank" data-pro="2" data-player="3" data-noob="1" href="http://uk.ign.com/videos/2016/03/18/dark-souls-prepare-to-try-episode-3-the-undead-parish" class="Grid-item Grid-item--small Grid-try3" data-video="true" noobfinish>
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--feature">
              <h3 class="Grid-category">Feature</h3>
              <h2 class="Grid-title">IGN Noob vs. Dark Souls - Episode 3: The Undead Parish</h2>
          </div></a><a target="_blank" data-pro="1" data-player="2" data-noob="3" href="http://uk.ign.com/videos/2016/03/20/dark-souls-prepare-to-try-episode-4-the-lower-undead-burg-capra-demon" data-video="true" class="Grid-item Grid-item--big Grid-try4" noobfinish>
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">IGN Noob vs. Dark Souls - Episode 4: The Lower Undead Burg</h2>
                <div class="Grid-text">We're rung one bell, so we're descending into the lower Undead Burg to meet some thieves and old goat face himself, the Capra Demon.</div>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="1" href="http://uk.ign.com/wikis/dark-souls-3/PC_Configuration" class="Grid-item Grid-item--small Grid-pc-settings" noobfinish>
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">See Dark Souls III's Detailed PC Graphics Settings</h2>
            </div></a><a target="_blank" data-pro="1" data-player="1" data-noob="50" href="http://uk.ign.com/wikis/dark-souls-3/Burial_Gifts" class="Grid-item Grid-item--small Grid-burial" noobfinish>
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--wiki">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Which Burial Gift Would You Choose in Dark Souls III?</h2>
            </div></a><a target="_blank" data-pro="1" data-player="2" data-noob="1" href="http://uk.ign.com/articles/2016/03/14/9-weirdest-ways-people-have-played-dark-souls?watch" class="Grid-item Grid-item--small Grid-weirdestways" noobfinish>
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">9 Weirdest Ways People Have Played Dark Souls</h2>
            </div></a><a target="_blank" data-pro="2" data-player="2" data-noob="70" href="http://uk.ign.com/videos/2016/03/02/dark-souls-3-brings-back-an-old-friend" class="Grid-item Grid-item--small Grid-oldfriend" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Dark Souls 3 Brings Back an Old Friend</h2>
            </div></a><a target="_blank" data-pro="1" data-player="4" data-noob="4" href="http://uk.ign.com/articles/2016/03/01/dark-souls-3-starting-classes-concept-art-revealed" class="Grid-item Grid-item--small Grid-conceptart">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--gallery">
                <h3 class="Grid-category">Gallery</h3>
                <h2 class="Grid-title">Dark Souls III Starting Classes Concept Art Revealed</h2>
            </div></a><a target="_blank" data-pro="1" data-player="2" data-noob="3" href="http://uk.ign.com/videos/2016/03/02/every-dark-souls-3-class" class="Grid-item Grid-item--big Grid-classes" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--preview">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">Every Dark Souls III Class</h2>
                <div class="Grid-text">Dark Souls 3 has 10 starting classes: Knight, Mercenary, Warrior, Thief, Sorcerer, Pyromancer, Cleric, Deprived, and the new Herald and Assassin.</div>
            </div></a><a target="_blank" data-pro="2" data-player="2" data-noob="2" href="http://www.ign.com/videos/2016/03/04/the-best-things-about-dark-souls-3-so-far" class="Grid-item Grid-item--small Grid-bestthing" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--opinion">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">The Best Things About Dark Souls III So Far</h2>
            </div></a><a target="_blank" data-pro="2" data-player="2" data-noob="2" href="http://uk.ign.com/videos/2016/03/04/dark-souls-3-11-gruesome-and-silly-deaths" class="Grid-item Grid-item--small Grid-sillydeaths" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">11 Gruesome and Silly Deaths on Dark Souls III</h2>
            </div></a><a target="_blank" data-pro="60" data-player="60" data-noob="1" href="http://uk.ign.com/articles/2016/03/16/prepare-to-try-ign-noob-takes-on-dark-souls?watch" class="Grid-item Grid-item--small Grid-noobfinish" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Can a Noob Finish Dark Souls Before Dark Souls III Comes Out?</h2>
            </div></a><a target="_blank" data-pro="3" data-player="3" data-noob="50" href="http://www.ign.com/videos/2015/12/05/breaking-down-dark-souls-3s-most-messed-up-boss" class="Grid-item Grid-item--small Grid-opinion" data-video="true">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--preview">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">Breaking Down Dark Souls 3's Most Messed-Up Boss</h2>
            </div></a><a target="_blank" data-pro="3" data-player="1.5" data-noob="1" href="http://uk.ign.com/articles/2016/03/01/19-details-fans-will-want-to-know-about-dark-souls-3" class="Grid-item Grid-item--big Grid-throne">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">19 Cool Details on Dark Souls III</h2>
                <div class="Grid-text">Something old, something new.</div>
            </div></a><a target="_blank" data-pro="6" data-player="6" data-noob="6" href="http://uk.ign.com/articles/2016/02/26/this-dark-souls-3-guide-comes-with-a-flask" class="Grid-item Grid-item--small Grid-levelUp">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Dark Souls III Guide Comes with a Flask</h2>
            </div></a><a target="_blank" data-pro="7" data-player="7" data-noob="7" href="http://www.ign.com/videos/2015/12/04/dark-souls-3-has-the-craziest-souls-boss-yet" data-video="true" class="Grid-item Grid-item--small Grid-chooseClass">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--opinion">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">Dark Souls III Has The Craziest Souls Boss Yet</h2>
            </div></a><a target="_blank" data-pro="8" data-player="8" data-noob="8" href="http://uk.ign.com/articles/2013/11/22/dark-souls-inspired-ps4s-design" class="Grid-item Grid-item--small Grid-item--margin Grid-getLevel">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--news">
                <h3 class="Grid-category">News</h3>
                <h2 class="Grid-title">Dark Souls Inspired PS4's Design</h2>
            </div></a><a target="_blank" data-pro="10" data-player="10" data-noob="50" href="http://uk.ign.com/articles/2015/09/17/tgs-2015-magic-makes-dark-souls-3-even-harder?watch" class="Grid-item Grid-item--small Grid-grinding">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">Dark Souls III Reinvents How Magic Works</h2>
            </div></a><a target="_blank" data-pro="4" data-player="4" data-noob="4" href="http://uk.ign.com/articles/2016/02/28/dark-souls-3-publisher-launching-slashy-souls-tomorrow" class="Grid-item Grid-item--small Grid-taken">
            <div class="Grid-layer Grid-shadow"></div>
            <div class="Grid-desc Grid-desc--news Grid-desc--maxi">
              <h3 class="Grid-category">News</h3>
              <h2 class="Grid-title">Bandai Namco Launches Mobile Game, Slashy Souls</h2>
          </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="10" href="http://www.ign.com/videos/2015/10/23/7-minutes-of-dark-souls-3-co-op-gameplay" data-video="true" class="Grid-item Grid-item--small Grid-coop">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--preview">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">7 Minutes of Dark Souls III Co-Op Gameplay</h2>
            </div></a><a target="_blank" data-pro="11" data-player="11" data-noob="11" href="http://uk.ign.com/videos/2016/02/08/dark-souls-iii-official-opening-cinematic-trailer" data-video="true" class="Grid-item Grid-item--small Grid-howTrials">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--trailer Grid-desc--maxi">
                <h3 class="Grid-category">Trailer</h3>
                <h2 class="Grid-title">Dark Souls III Opening Cinematic Trailer</h2>
            </div></a><a target="_blank" data-pro="5" data-player="5" data-noob="5" href="http://uk.ign.com/videos/2016/02/24/dark-souls-3-official-true-colors-of-darkness-trailer" class="Grid-item Grid-item--big Grid-item--margin Grid-tipsTricks" data-video="true">
                <div class="Grid-layer Grid-shadow"></div>
                <div class="Grid-desc Grid-desc--wiki">
                  <h3 class="Grid-category">Wiki</h3>
                  <h2 class="Grid-title">True Colours of Darkness Trailer</h2>
                  <div class="Grid-text">Bandai Namco shows off new gameplay of the action-RPG, with a stirring rendition of "True Colors" serving as the musical backdrop.</div>
              </div></a><a target="_blank" data-pro="11" data-player="11" data-noob="11" href="http://uk.ign.com/articles/2015/03/16/the-history-of-from-software" class="Grid-item Grid-item--big Grid-item--margin Grid-factions">
              <div class="Grid-layer Grid-shadow"></div>
              <div class="Grid-desc Grid-desc--feature">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">The History of From Software</h2>
                <div class="Grid-text">From King's Field to Dark Souls.</div>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="12" href="http://www.ign.com/videos/2015/10/23/watch-4-minutes-of-dark-souls-3s-sorcery-class-in-action" data-video="true" class="Grid-item Grid-item--small Grid-fiveWays">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">Dark Souls III's Sorcery Class in Action</h2>
            </div></a><a target="_blank" data-pro="50" data-player="13" data-noob="13" href="http://www.ign.com/videos/2015/09/17/4-minutes-of-brutal-new-dark-souls-3-gameplay-tgs-2015" data-video="true" class="Grid-item Grid-item--small Grid-101">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">4 Minutes of Brutal Dark Souls III Gameplay</h2>
            </div></a><a target="_blank" data-pro="14" data-player="14" data-noob="14" href="http://uk.ign.com/articles/2016/01/28/stunning-dark-souls-3-screens-show-more-of-the-world" class="Grid-item Grid-item--small Grid-level20">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--gallery Grid-desc--maxi">
                <h3 class="Grid-category">Gallery</h3>
                <h2 class="Grid-title">Stunning Dark Souls 3 Screens Show More of the World</h2>
            </div></a><a target="_blank" data-pro="15" data-player="50" data-noob="50" href="http://uk.ign.com/videos/2015/08/06/dark-souls-3-gamescom-trailer-rewind" data-video="true" class="Grid-item Grid-item--small Grid-bounties">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">Terrifying Things Trailer Rewind</h2>
            </div></a><a target="_blank" data-pro="16" data-player="16" data-noob="16" href="http://uk.ign.com/videos/2015/08/04/dark-souls-3-terrifying-things-trailer-gamescom-2015" data-video="true" class="Grid-item Grid-item--small Grid-xur">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--trailer Grid-desc--maxi">
                <h3 class="Grid-category">Trailer</h3>
                <h2 class="Grid-title">Terrifying Things Trailer</h2>
            </div></a><a target="_blank" data-pro="17" data-player="17" data-noob="17" href="http://uk.ign.com/videos/2015/12/04/dark-souls-3-darkness-has-spread-trailer" data-video="true" class="Grid-item Grid-item--small Grid-darkness">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--trailer Grid-desc--maxi">
                <h3 class="Grid-category">Trailer</h3>
                <h2 class="Grid-title">Darkness Has Spread Trailer</h2>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="18" href="http://uk.ign.com/articles/2014/03/11/dark-souls-2-review" class="Grid-item Grid-item--small Grid-vault">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--review Grid-desc--maxi">
                <h3 class="Grid-category">Review</h3>
                <h2 class="Grid-title">Dark Souls II Review</h2>
            </div></a><a target="_blank" data-pro="50" data-player="19" data-noob="19" href="http://uk.ign.com/wikis/dark-souls-2/Top_7_Characters_In_Dark_Souls_2?objectid=149844" class="Grid-item Grid-item--big Grid-5mins">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Top 7 Characters in Dark Souls II</h2>
                <div class="Grid-text">Here we have compiled a list of the 7 most interesting, unique and strange characters you can find in the game.</div>
            </div></a><a target="_blank" data-pro="50" data-player="20" data-noob="20" href="http://uk.ign.com/wikis/dark-souls-2/Dark_Souls_vs_Dark_Souls_2?objectid=149844" class="Grid-item Grid-item--big Grid-compare">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Dark Souls vs Dark Souls II</h2>
                <div class="Grid-text">Dark Souls 2 is different than Dark Souls in seven fundamental ways</div>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="21" href="http://uk.ign.com/wikis/dark-souls-2/Tips,_Tricks,_and_Tactics?objectid=149844" class="Grid-item Grid-item--small Grid-raids">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--wiki Grid-desc--maxi">
                <h3 class="Grid-category">Wiki</h3>
                <h2 class="Grid-title">Dark Souls II Starter Guide</h2>
            </div></a><a target="_blank" data-pro="22" data-player="22" data-noob="50" href="http://uk.ign.com/articles/2014/10/09/the-final-word-on-dark-souls-2s-toughest-boss" class="Grid-item Grid-item--small Grid-armour">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Dark Souls II's Toughest Bosses Revealed</h2>
            </div></a><a target="_blank" data-pro="23" data-player="23" data-noob="50" href="http://uk.ign.com/articles/2014/10/16/take-a-closer-look-at-the-art-of-dark-souls-ii" class="Grid-item Grid-item--small Grid-players">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--gallery Grid-desc--maxi">
                <h3 class="Grid-category">Gallery</h3>
                <h2 class="Grid-title">The Art of Dark Souls II</h2>
            </div></a><a target="_blank" data-pro="50" data-player="24" data-noob="24" href="http://uk.ign.com/articles/2014/05/06/understanding-the-lore-of-dark-souls-2" class="Grid-item Grid-item--small Grid-raids2">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Understanding the Lore of Dark Souls II</h2>
            </div></a><a target="_blank" data-pro="25" data-player="25" data-noob="25" href="http://uk.ign.com/articles/2014/02/21/the-endless-endgame-of-dark-souls" class="Grid-item Grid-item--small Grid-shirpa">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--opinion Grid-desc--maxi">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">Why Dark Souls Only Begins When You Finish It</h2>
            </div></a><a target="_blank" data-pro="26" data-player="26" data-noob="26" href="http://uk.ign.com/articles/2014/03/03/making-dark-souls-ii" class="Grid-item Grid-item--small Grid-making">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--feature Grid-desc--maxi">
                <h3 class="Grid-category">Feature</h3>
                <h2 class="Grid-title">Making Dark Souls II</h2>
            </div></a><a target="_blank" data-pro="50" data-player="50" data-noob="27" href="http://uk.ign.com/articles/2011/09/30/dark-souls-review" class="Grid-item Grid-item--small Grid-review">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--review Grid-desc--maxi">
                <h3 class="Grid-category">Review</h3>
                <h2 class="Grid-title">Dark Souls Review</h2>
            </div></a><a target="_blank" data-pro="9" data-player="9" data-noob="9" href="http://uk.ign.com/videos/2015/08/11/what-does-the-dark-souls-3-energy-drink-taste-like" data-video="true" class="Grid-item Grid-item--small Grid-drink">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--opinion Grid-desc--maxi">
                <h3 class="Grid-category">Opinion</h3>
                <h2 class="Grid-title">What Does the Dark Souls III Energy Drink Taste Like?</h2>
            </div></a><a target="_blank" data-pro="2" data-player="2" data-noob="1" href="http://uk.ign.com/videos/2016/03/01/the-first-14-minutes-of-dark-souls-3" data-video="true" class="Grid-item Grid-item--small Grid-first14">
              <div class="Grid-layer"></div>
              <div class="Grid-desc Grid-desc--preview Grid-desc--maxi">
                <h3 class="Grid-category">Preview</h3>
                <h2 class="Grid-title">The First 14 Minutes of Dark Souls III</h2>
            </div></a></div>



        </div>


        <footer class="tab-bar">
          <div class="row">
            <div class="ign_red-wrapper wrapper-dark">
              <div class="title"><img src="dist/build/img/boilerplate/zd_logo.png" width="204" height="77"></div>
              <ul>
                <li><a href="http://corp.ign.com/privacy.html" title="Privacy Policy">Privacy policy</a></li>
                <li><a href="http://corp.ign.com/user-agreement.html" title="User Agreement">User agreement</a></li>
              </ul>
            </div>
            <ul class="left links">
              <ul>
                <li>
                  <p>Copyright 2016</p>
                  <p class="nomargin">Ziff Davis International Ltd</p>
                </li>
                <li><a href="http://uk.corp.ign.com/#about" title="About Us">About Us</a></li>
                <li><a href="http://uk.corp.ign.com/#contact" title="Contact Us">Contact Us</a></li>
                <li><a href="http://corp.ign.com/feeds.html" title="RSS Feeds">RSS Feeds</a></li>
              </ul>
            </ul>
          </div>
        </footer><a class="exit-off-canvas"></a>
        <div id="videoLightbox" class="lightbox">
          <div id="lightboxContent" class="content">
            <div class="video-wrapper">
              <iframe id="videoPlayer" src="" width="468" height="263" scrolling="no" frameborder="0" allowfullscreen></iframe>
            </div><a href="#" class="close">&times;</a>
          </div>
        </div>

        <!-- <script type="text/javascript" src="http://geobeacon.ign.com/geodetect.js"></script> -->

        <script type="text/javascript" src="dist/build/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="dist/build/isotope.js"></script>
        <script type="text/javascript" src="dist/build/packery.js"></script>
        <script src="dist/build/build.min.js"></script>

        <script>

           $(document).ready(function(){
              var $grid = $('.Page-grid').isotope({
                layoutMode: 'packery',
                itemSelector: '.Grid-item',
                packery: {
                     gutter: 10
                },
                getSortData: {
                     pro: '[data-pro]',
                     player: '[data-player]',
                     noob: '[data-noob]'
                },
                sortBy: 'noob'
              });

              // bind filter button click
              $('#levelMap').on('click', 'a', function () {
                var sortBy = $(this).attr('data-sort');

                $grid.isotope({
                     sortBy: sortBy,
                     sortAscending: true
                });
              });
           });

        </script>


        <script>

          (function(){
              var cookieName = 'persist-policy';
              var cookieValue = 'eu';
              var createCookie = function(name,value,days,domain) {
                  var expires = '';
                  var verifiedDomain = '';
                  if (days) {
                      var date = new Date();
                      date.setTime(date.getTime()+(days*24*60*60*1000));
                      expires = '; expires='+date.toGMTString();
                  }
                  if (domain) {
                      verifiedDomain = '; domain='+domain;
                  }
                  document.cookie = name+'='+value+expires+verifiedDomain+'; path=/';
              };
              var readCookie = function(name) {
                  var nameEQ = name + "=";
                  var ca = document.cookie.split(';');
                  for(var i=0;i < ca.length;i++) {
                      var c = ca[i];
                      while (c.charAt(0)==' ') c = c.substring(1,c.length);
                      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                  }
                  return null;
              };

              window.initPolicyWidget = function(){
                  jQuery('#policyNotice').show().find('.close-btn a').click(function(e){
                      createCookie(cookieName, cookieValue, 180, jQuery(this).data('domain'));
                      jQuery('#policyNotice').remove();
                      return false;
                  });
              }
              var cookieContent = readCookie(cookieName);
              if (typeof  cookieContent === 'undefined' || cookieContent != cookieValue) {
                  jQuery(document).ready(initPolicyWidget);
                  jQuery(document).trigger('policyWidgetReady');
              }
          })();

      </script>


        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-15279170-1']);
            _gaq.push(['_trackPageview', 'darksouls_archives_tracking']);

            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
      </div>
    </div>
  </body>
</html>
